package com.devcamp.cartype.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cartype.model.CCar;

public interface ICarRepository extends JpaRepository<CCar, Integer> {
    CCar findByCarCode(String car);
}
