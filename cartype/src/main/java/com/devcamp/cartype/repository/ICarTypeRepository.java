package com.devcamp.cartype.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.cartype.model.CCarType;

public interface ICarTypeRepository extends JpaRepository<CCarType, Integer>{
    
}
