package com.devcamp.cartype.service;

import java.util.ArrayList;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.cartype.model.CCar;
import com.devcamp.cartype.model.CCarType;
import com.devcamp.cartype.repository.ICarRepository;



@Service
public class CarService {
    @Autowired
    ICarRepository pCarRepository;

    public ArrayList<CCar> getAllCars() {
        ArrayList<CCar> listCar = new ArrayList<>();
        pCarRepository.findAll().forEach(listCar::add);
        return listCar;
    }

    public Set<CCarType> getCarTypeByCarCode(String countryCode) { 
        CCar vCar = pCarRepository.findByCarCode(countryCode);
        if (vCar != null) {
            return  vCar.getCarTypes();
        } else {
            return null;
        }
    }
}
