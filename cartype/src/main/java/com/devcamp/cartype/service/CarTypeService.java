package com.devcamp.cartype.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.cartype.model.CCarType;
import com.devcamp.cartype.repository.ICarTypeRepository;

@Service
public class CarTypeService {
    @Autowired
    ICarTypeRepository pCarTypeRepository;

    public ArrayList<CCarType> getAllCarTypes() {
        ArrayList<CCarType> types = new ArrayList<>();
        
        List<CCarType> pTypes = new ArrayList<CCarType>();

		pCarTypeRepository.findAll().forEach(pTypes::add);

        return types;
    }
}
