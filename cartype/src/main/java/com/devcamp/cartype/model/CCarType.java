package com.devcamp.cartype.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "car_type")
public class CCarType {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int type_id;

    @Column(name = "type_code", unique = true)
    private String typeCode;

    @Column(name = "type_name")
    private String typeName;
    
    @ManyToOne
    @JoinColumn(name = "car_id")
    @JsonBackReference

    private CCar car;


    public CCarType() {
    }

    public CCarType(int type_id, String typeCode, String typeName) {
        this.type_id = type_id;
        this.typeCode = typeCode;
        this.typeName = typeName;
    }

    public int getType_id() {
        return type_id;
    }

    public void setType_id(int type_id) {
        this.type_id = type_id;
    }

    public String getTypeCode() {
        return typeCode;
    }

    public void setTypeCode(String typeCode) {
        this.typeCode = typeCode;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }


    /**
	 * @return the country
	 */

     public CCar getCar(){
        return car;
     }
    
     /**
	 * @param car the country to set
	 */
    public void setCar(CCar car) {
		this.car = car;
	}
    
}
