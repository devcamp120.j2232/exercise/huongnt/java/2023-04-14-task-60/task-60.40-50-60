package com.devcamp.cartype;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CartypeApplication {

	public static void main(String[] args) {
		SpringApplication.run(CartypeApplication.class, args);
	}

}
