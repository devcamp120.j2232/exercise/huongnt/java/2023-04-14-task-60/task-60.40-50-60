package com.devcamp.cartype.controller;

import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.cartype.model.CCar;
import com.devcamp.cartype.model.CCarType;
import com.devcamp.cartype.service.CarService;
import com.devcamp.cartype.service.CarTypeService;



@RestController
@RequestMapping("/")
@CrossOrigin(value = "*", maxAge = -1)
public class CarController {
    @Autowired
    private CarService carService;

    // get car list
    @GetMapping("/cars")
	public ResponseEntity<List<CCar>> getAllCarsApi() {
		try {
			return new ResponseEntity<>(carService.getAllCars(), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

        // lấy danh sách car typetruyền vào car code 
        @GetMapping("/car-type")
        public ResponseEntity<Set<CCarType>> getCarsByCarCode(@RequestParam(value = "carCode") String carCode) {
            try {
                Set<CCarType> types = carService.getCarTypeByCarCode(carCode);
                if (types != null) {
                    return new ResponseEntity<>(types, HttpStatus.OK);
                } else {
                    return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
                }
            } catch (Exception e) {
                return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
            }
        }
    
    
}
